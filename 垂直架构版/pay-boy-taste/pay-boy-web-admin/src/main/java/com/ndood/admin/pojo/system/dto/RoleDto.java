package com.ndood.admin.pojo.system.dto;

import java.util.List;

import com.ndood.admin.pojo.system.RolePo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 角色DTO类
 */
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
public class RoleDto extends RolePo {
	private static final long serialVersionUID = 6650285921381454775L;
	private List<Integer> resourceIds;
	/**
	 * 用来判断是否选中
	 */
	private Boolean isInChecked;
}

package com.ndood.admin.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.dto.DictDto;
import com.ndood.admin.pojo.system.query.DictQuery;
import com.ndood.admin.service.system.SystemDictService;

/**
 * 字典控制器类
 * @author ndood
 */
@Controller
public class SystemDictController {
	
	@Autowired
	private SystemDictService systemDictService;

	/**
	 * 显示字典页
	 */
	@GetMapping("/system/dict")
	public String toDictPage(){
		return "system/dict/dict_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/dict/add")
	public String toAddDict(){
		return "system/dict/dict_add";
	}
	
	/**
	 * 添加字典
	 */
	@PostMapping("/system/dict/add")
	@ResponseBody
	public AdminResultVo addDict(@RequestBody DictDto dict) throws Exception{
		systemDictService.addDict(dict);
		return AdminResultVo.ok().setMsg("添加字典成功！");
	}
	
	/**
	 * 删除字典
	 */
	@PostMapping("/system/dict/delete")
	@ResponseBody
	public AdminResultVo deleteDict(Integer id){
		Integer[] ids = new Integer[]{id};
		systemDictService.batchDeleteDict(ids);
		return AdminResultVo.ok().setMsg("删除字典成功！");
	}

	/**
	 * 添加字典
	 */
	@PostMapping("/system/dict/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteDict(@RequestParam("ids[]") Integer[] ids){
		systemDictService.batchDeleteDict(ids);
		return AdminResultVo.ok().setMsg("批量删除字典成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/dict/update")
	public String toUpdateDict(Integer id, Model model) throws Exception{
		DictDto dict = systemDictService.getDict(id);
		model.addAttribute("dict", dict);
		return "system/dict/dict_update";
	}
	
	/**
	 * 修改字典
	 */
	@PostMapping("/system/dict/update")
	@ResponseBody
	public AdminResultVo updateDict(@RequestBody DictDto dict) throws Exception{
		systemDictService.updateDict(dict);
		return AdminResultVo.ok().setMsg("修改字典成功！");
	}
	
	/**
	 * 字典列表
	 */
	@PostMapping("/system/dict/query")
	@ResponseBody
	public DataTableDto getDectListPage(@RequestBody DictQuery query) throws Exception{
		DataTableDto page = systemDictService.pageDictList(query);
		return page;
	}
	
}

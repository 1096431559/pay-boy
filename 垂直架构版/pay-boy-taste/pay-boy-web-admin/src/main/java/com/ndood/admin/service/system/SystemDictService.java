package com.ndood.admin.service.system;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.dto.DictDto;
import com.ndood.admin.pojo.system.query.DictQuery;

/**
 * 字典管理业接口
 * @author ndood
 */
public interface SystemDictService {

	/**
	 * 字典列表
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	DataTableDto pageDictList(DictQuery query) throws Exception;

	/**
	 * 添加字典
	 * @param dict
	 * @throws Exception 
	 */
	void addDict(DictDto dict) throws Exception;

	/**
	 * 批量删除字典
	 * @param ids
	 */
	void batchDeleteDict(Integer[] ids);

	/**
	 * 更新字典
	 * @param dict
	 * @throws Exception 
	 */
	void updateDict(DictDto dict) throws Exception;

	/**
	 * 获取单个字典
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	DictDto getDict(Integer id) throws Exception;

}

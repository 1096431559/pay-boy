package com.ndood.core.social.qq.connect;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;

import com.ndood.core.social.qq.api.QQ;

/**
 * 添加QQConnectionFactory
 */
public class QQConnectionFactory extends OAuth2ConnectionFactory<QQ>{

	/**
	 * 构造方法
	 * @param providerId
	 * @param appId
	 * @param appSecret
	 */
	public QQConnectionFactory(String providerId, String appId, String appSecret) {
		super(providerId, new QQServiceProvider(appId, appSecret), new QQAdapter());
	}
	
}

package com.ndood.core.properties;

/**
 * Oauth2 client相关配置
 */
public class Oauth2ClientProperties {
	private String clientId = "ndood";

	private String clientSecret = "ndood";
	
	private String grantType = "password";

	private int accessTokenValiditySeconds; // 等0默认不过期

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public int getAccessTokenValiditySeconds() {
		return accessTokenValiditySeconds;
	}

	public void setAccessTokenValiditySeconds(int accessTokenValiditySeconds) {
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
	}

	public String getGrantType() {
		return grantType;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	
}

package com.sdk.wxpay.addons;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import com.sdk.wxpay.IWXPayDomain;
import com.sdk.wxpay.WXPayConfig;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Configuration
@DependsOn({"dbGlobalConfig"})
public class LocalWXPayConfig extends WXPayConfig{
	
	/**
	 * 微信支付域名
	 */
	@Autowired
	private IWXPayDomain wxPayDomain;
	/**
	 * 微信支付appid
	 */
	@Value("${ndood.payment.global-config.app-id:}")
	private String appId;
	/**
	 * 微信支付商户id
	 */
	@Value("${ndood.payment.global-config.mch-id:}")
	private String mchId;
	/**
	 * 微信支付api秘钥
	 */
	@Value("${ndood.payment.global-config.key:}")
	private String key;
	/**
	 * 微信支付证书字节流
	 */
	@Value("${ndood.payment.global-config.cert-stream:}")
	private String certStream;
	/**
	 * 测试附加参数
	 */
	@Value("${ndood.payment.global-config.url:}")
	private String url;
	
	@Override
	public String getAppID() {
		return appId;
	}

	@Override
	public String getMchID() {
		return mchId;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public InputStream getCertStream() {
		return new ByteArrayInputStream(certStream.getBytes());
	}

	@Override
	public IWXPayDomain getWXPayDomain() {
		return wxPayDomain;
	}
	
}

package com.ndood.payment.controller.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdk.wxpay.addons.LocalWXPayConfig;

/**
 * 模拟控制器
 */
@Controller
@RequestMapping("/v1/mock")
public class MockController {
	
	@Autowired
	private LocalWXPayConfig localWXPayConfig;
	
	@GetMapping("/f2f")
	public String hello() {
		System.out.println(localWXPayConfig.getUrl());
		return "mock/f2f";
	}
	
}
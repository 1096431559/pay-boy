package com.ndood.payment.core.constaints;

public enum PaymentCode {
	
	SUCCESS(10000, "请求成功"),
	ERR_PARAMS(10001,"请求参数错误"),
	ERR_SERVER(10002,"系统错误"), 
	ERR_TOKEN(10003,"无效的token");
	
    private int code;
    private String value;
    
    private PaymentCode(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
    
	public static PaymentCode getEnum(int code) {
		for (PaymentCode rs : PaymentCode.values()) {
			if (code==rs.getCode()) {
				return rs;
			}
		}
		return PaymentCode.ERR_SERVER;
	}
    
}


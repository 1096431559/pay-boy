package com.ndood.payment.core.properties;

/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(prefix = "ndood.payment")
@Data
public class PaymentProperties {
	/**
	 * 系统默认域名
	 */
	private String domain = "http://pay.jeepupil.top";
	/**
	 * 默认的sessionid名称
	 */
	private String DefaultCookieName = "JSESSIONID";
	/**
	 * 是否开发模式
	 */
	private Boolean isDevelop = false;
	/**
	 * 是否启用数据库配置
	 */
	private Boolean isDbGlobalConfig = false;
	/**
	 * 默认全局配置信息
	 */
	/**
	 * 支付相关配置
	 */
	private PayConfigProperties pay = new PayConfigProperties();
	/**
	 * oss相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
}
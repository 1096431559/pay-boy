package com.ndood.payment.core.cache;

import java.time.Duration;
import java.util.Map;
import java.util.Set;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * SpringCache缓存配置类
 */
@Configuration
public class SpringCacheConfig extends CachingConfigurerSupport {
	
	@Bean
	public CacheManager cacheManager(RedisConnectionFactory factory) {
		
		// 配置策略
		RedisCacheConfiguration cacheConfiguration30m = RedisCacheConfiguration.defaultCacheConfig()
				.entryTtl(Duration.ofMinutes(30))// 缓存7天
				.disableCachingNullValues()
				.computePrefixWith(cacheName -> "buiz_cache".concat(":").concat(cacheName).concat(":"))
				.serializeValuesWith(RedisSerializationContext.SerializationPair
						.fromSerializer(new GenericJackson2JsonRedisSerializer()));
		
		// 设置一个初始化的缓存空间set集合
		Set<String> cacheNames = Sets.newHashSet();
		cacheNames.add("merchant");

		// 对每个缓存空间应用不同的配置
		Map<String, RedisCacheConfiguration> configMap = Maps.newHashMap();
		configMap.put("merchant", cacheConfiguration30m);
		
		RedisCacheManager cacheManager = RedisCacheManager.builder(factory) // 使用自定义的缓存配置初始化一个cacheManager
				.initialCacheNames(cacheNames) // 注意这两句的调用顺序，一定要先调用该方法设置初始化的缓存名，再初始化相关的配置
				.withInitialCacheConfigurations(configMap).build();
		
		return cacheManager;
		
	}

}
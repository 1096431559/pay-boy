/**
 * require.js总配置类
 */
requirejs.config({
	baseUrl: '/',
    waitSeconds: 30,
    charset: 'utf-8',
	//urlArgs: "bust=" + (new Date()).getTime(), // 记得部署到生产环境的时候移除掉
	urlArgs: 'v=20181127',
	map: {
        '*': {
            'css': './static/lib/require/css'
        }
    },
	paths: {
		'cookie': [ 
			'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.0.4/js.cookie.min',
			'lib/js.cookie.min',
		],
		'jquery': [
			'https://cdn.bootcss.com/jquery/3.3.1/jquery.min',
			'./static/lib/jquery.min',
		],
		'jquery-lazyload': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.3/jquery.lazyload',
			'./static/lib/jquery.lazyload.min',
		],
		'jquery-extension': './static/lib/jquery.extension',
		'jquery-validation': [
			'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min',
			'./static/lib/jquery-validation/js/jquery.validate.min',
		],
		'jquery-distselector': './static/lib/jquery-distselector/jquery-distselector',
		'bootstrap': [
			'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min',
			'./static/lib/bootstrap/js/bootstrap.min',
		],
		'bootstrap-datepicker': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min',
			'./static/lib/bootstrap-datepicker/js/bootstrap-datepicker',
		],
		'bootstrap-datepicker-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.zh-CN.min',
			'./static/lib/bootstrap-datepicker/js/bootstrap-datepicker.zh-CN',
		],
		'bootstrap-datetimepicker': [
			'https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/bootstrap-datetimepicker.min',
			'./static/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
		],
		'bootstrap-datetimepicker-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/locales/bootstrap-datetimepicker.zh-CN',
			'./static/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN',
		],
		'bootstrap-table': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min',
			'./static/lib/bootstrap-table/js/bootstrap-table',
		],
		'bootstrap-table-mobile': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/extensions/mobile/bootstrap-table-mobile.min',
			'./static/lib/bootstrap-table/js/bootstrap-table-mobile',
		],
		'bootstrap-table-zh-CN': [
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/locale/bootstrap-table-zh-CN.min',
			'./static/lib/bootstrap-table/js/bootstrap-table-zh-CN',
		],
		'bootstrap-imguploader': './static/lib/bootstrap-imguploader/bootstrap-imguploader',
		'layui': './static/lib/layer/layer',
		'app': './static/scripts/global/app',
		'layout': './static/scripts/global/layout',
		'quick-nav': './static/scripts/global/quick-nav',
		'vue': [
			'https://cdn.bootcss.com/vue/2.5.17-beta.0/vue.min',
			'./static/lib/vue/vue.min'
		],
		'vue-router':[
			'https://cdn.bootcss.com/vue-router/3.0.1/vue-router.min',
			'./static/lib/vue/vue-router.min'
		],
		'vue-resource':[
			'https://cdn.bootcss.com/vue-resource/1.5.1/vue-resource.min',
			'./static/lib/vue/vue-resource.min'
		],
		'vuex':[
			'https://cdn.bootcss.com/vuex/3.0.1/vuex.min',
			'./static/lib/vue/vuex.min'
		],
	},
	shim: {
		'cookie':['jquery'],
		'jquery-lazyload': ['jquery'],
		'jquery-validation': {
			deps: ['jquery'],
			exports: '$.validator'
		},
		'jquery-extension': {
			deps: ['jquery','jquery-validation'],
		},
		'bootstrap': ['jquery'],
		'bootstrap-table': {
			deps: ['jquery','bootstrap'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-zh-CN': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-table-mobile': {
			deps: ['jquery','bootstrap-table'],
			exports: '$.fn.bootstrapTable'
		},
		'bootstrap-datepicker': ['jquery','bootstrap'],
		'bootstrap-datepicker-zh-CN': ['jquery','bootstrap-datepicker'],
		'bootstrap-datetimepicker': ['jquery','bootstrap'],
		'bootstrap-datetimepicker-zh-CN': ['jquery','bootstrap-datetimepicker'],
		'bootstrap-imguploader':['jquery'],
		'layui':['jquery'],
		'app':['jquery','bootstrap'],
		'layout':['jquery','app'],
		'quick-nav':['jquery','app'],
		'vue': {
			exports: 'Vue'
		},
		'vue-router': {
			deps: ['vue'],
		},
		'vuex': {
			deps: ['vue'],
		},
		'vue-resource': {
			deps: ['vue'],
		},
	},
	packages: [
    	{
    		name: 'asserts',
    		location: './static/asserts',
    	},
    	{
    		name : "components",
    		location :"./static/components",
    	},
    	{
            name: 'lib',
            location: './static/lib',
        },
        {
            name: 'scripts',
            location: './static/scripts',
        },
        {
            name: 'vuex',
            location: './static/vuex',
        },
    ],
});

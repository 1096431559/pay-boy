package com.ndood.merchant.core.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.ndood.merchant.core.properties.MerchantProperties;

/**
 * 初始化的时候配置默认的sessionid名称
 * 配置了以后使用EditThisCookie后看到的还是JSESSIONID，但2个系统已经不冲突了
 */
@Component
public class CookieNameInitializer {
	
	@Autowired
	private MerchantProperties merchantProperties;
	
	@Bean
	public ServletContextInitializer servletContextInitializer() {
	    return new ServletContextInitializer() {
	        @Override
	        public void onStartup(ServletContext servletContext) throws ServletException {
	            servletContext.getSessionCookieConfig().setName(merchantProperties.getDefaultCookieName());
	        }
	    };
	}
}


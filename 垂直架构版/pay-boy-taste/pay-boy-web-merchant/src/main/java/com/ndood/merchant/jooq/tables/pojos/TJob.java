/*
 * This file is generated by jOOQ.
*/
package com.ndood.merchant.jooq.tables.pojos;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.8"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TJob implements Serializable {

    private static final long serialVersionUID = -19047716;

    private Integer       id;
    private String        cronExpression;
    private String        methodName;
    private String        isConcurrent;
    private String        description;
    private String        updateBy;
    private String        beanClass;
    private String        jobStatus;
    private String        jobGroup;
    private String        createBy;
    private String        springBean;
    private String        jobName;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

    public TJob() {}

    public TJob(TJob value) {
        this.id = value.id;
        this.cronExpression = value.cronExpression;
        this.methodName = value.methodName;
        this.isConcurrent = value.isConcurrent;
        this.description = value.description;
        this.updateBy = value.updateBy;
        this.beanClass = value.beanClass;
        this.jobStatus = value.jobStatus;
        this.jobGroup = value.jobGroup;
        this.createBy = value.createBy;
        this.springBean = value.springBean;
        this.jobName = value.jobName;
        this.createTime = value.createTime;
        this.updateTime = value.updateTime;
    }

    public TJob(
        Integer       id,
        String        cronExpression,
        String        methodName,
        String        isConcurrent,
        String        description,
        String        updateBy,
        String        beanClass,
        String        jobStatus,
        String        jobGroup,
        String        createBy,
        String        springBean,
        String        jobName,
        LocalDateTime createTime,
        LocalDateTime updateTime
    ) {
        this.id = id;
        this.cronExpression = cronExpression;
        this.methodName = methodName;
        this.isConcurrent = isConcurrent;
        this.description = description;
        this.updateBy = updateBy;
        this.beanClass = beanClass;
        this.jobStatus = jobStatus;
        this.jobGroup = jobGroup;
        this.createBy = createBy;
        this.springBean = springBean;
        this.jobName = jobName;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCronExpression() {
        return this.cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getIsConcurrent() {
        return this.isConcurrent;
    }

    public void setIsConcurrent(String isConcurrent) {
        this.isConcurrent = isConcurrent;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getBeanClass() {
        return this.beanClass;
    }

    public void setBeanClass(String beanClass) {
        this.beanClass = beanClass;
    }

    public String getJobStatus() {
        return this.jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobGroup() {
        return this.jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getCreateBy() {
        return this.createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getSpringBean() {
        return this.springBean;
    }

    public void setSpringBean(String springBean) {
        this.springBean = springBean;
    }

    public String getJobName() {
        return this.jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public LocalDateTime getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("TJob (");

        sb.append(id);
        sb.append(", ").append(cronExpression);
        sb.append(", ").append(methodName);
        sb.append(", ").append(isConcurrent);
        sb.append(", ").append(description);
        sb.append(", ").append(updateBy);
        sb.append(", ").append(beanClass);
        sb.append(", ").append(jobStatus);
        sb.append(", ").append(jobGroup);
        sb.append(", ").append(createBy);
        sb.append(", ").append(springBean);
        sb.append(", ").append(jobName);
        sb.append(", ").append(createTime);
        sb.append(", ").append(updateTime);

        sb.append(")");
        return sb.toString();
    }
}
